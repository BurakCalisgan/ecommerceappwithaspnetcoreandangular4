import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
//import '../rxjs/add/operator/map';
//import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";

@Injectable()
export class ProductService {

    constructor(private _http: Http) { 

    }

    getProduct(){
        return this._http.get('https://localhost:44390/api/product')
        .pipe(map((res:Response) => res.json()));
    }
}