import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { DetailComponent } from '../app/pages/detail.component';
import { HomeComponent } from './pages/home.component';
import { HttpModule } from '@angular/http';
import { ProductService } from './services/product.service';
import { CartService } from './services/cart.service';
import { CartComponent } from './pages/cart.component';

const appRoutes: Routes = [
  {path: 'detail', component: DetailComponent},
  {path: 'cart', component: CartComponent},
  {path: '',component: HomeComponent},
  {path: '**',component: AppComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    HomeComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes, { enableTracing: true }
    )
  ],
  providers: [ProductService,CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
