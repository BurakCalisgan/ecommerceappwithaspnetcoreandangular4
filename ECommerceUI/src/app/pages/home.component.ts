import {Component, OnInit} from '@angular/core';
import {Product} from '../models/Product';
import {ProductService} from '../services/product.service';
import { CartService } from '../services/cart.service';
import { Cart } from '../models/Cart';

@Component({
    selector:'home-page',
    templateUrl:'home.component.html'
})

export class HomeComponent {
  cartList: Cart [] =  [];
  productList: Product [] = [];
  cartLength: number;

    
  constructor(private _productService : ProductService, private _cartService: CartService){
    _productService.getProduct().subscribe(data => this.productList = data);
   
  }

  addCart(product: Product){
    const item = this.cartList.find(x=>x.ProductId == product.productId);
    if(item != null){
      item.Count++;

    }
    else {
      this.cartList.push({'ProductName': product.name, 'ProductId': product.productId, 'Count' : 1});
    }
    this.cartLength = this.cartList.length;
    console.log(this.cartList);
    this._cartService.CartList = this.cartList;

  }

}