import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';

@Component({
    selector: 'cart',
    templateUrl: 'cart.component.html'
})

export class CartComponent implements OnInit {
    cartList;
    constructor(private _cartService: CartService) { 
        this.cartList = _cartService.CartList
    }

    ngOnInit() { }
}