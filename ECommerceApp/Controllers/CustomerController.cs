﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerceApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ECommerceApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ECommerceContext _context;
        public CustomerController(ECommerceContext context)
        {
            _context = context;
        }

        // GET api/Customer
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            return _context.Customers.ToList();
        }

        // GET api/Product/5
        [HttpGet("{id}")]
        public ActionResult<Customer> Get(int id)
        {
            return _context.Customers.FirstOrDefault(x => x.CustomerId == id);
        }

        // POST api/Customer
        [HttpPost]
        public void Post([FromBody] Customer customer)
        {
            _context.Customers.Add(customer);
            _context.SaveChanges();
        }

        // PUT api/Customer/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Customer customer)
        {
            var oldCustomer = _context.Customers.FirstOrDefault(x => x.CustomerId == id);
            oldCustomer.Name = customer.Name;
            oldCustomer.Address = customer.Address;
            oldCustomer.PhoneNumber = customer.PhoneNumber;
            oldCustomer.EMail = customer.EMail;
            _context.Customers.Update(oldCustomer);
            _context.SaveChanges();
        }

        // DELETE api/Customer/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var customer = _context.Customers.FirstOrDefault(x => x.CustomerId == id);
            _context.Customers.Remove(customer);
            _context.SaveChanges();
        }
    }
}