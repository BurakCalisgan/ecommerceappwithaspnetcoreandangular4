﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerceApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace ECommerceApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ECommerceContext _context;
        public ProductController(ECommerceContext context)
        {
            _context = context;
        }
        //List<Product> productList = new List<Product>()
        //{
        //    new Product { Name = "Bilgisayar", Price= 1000, ImgUrl="https://images-eu.ssl-images-amazon.com/images/G/41/Launch/Computers/NAV/116655_Computers_12466440031_LaptopsDesktops_440x480.jpg"},
        //    new Product { Name = "Klavye", Price= 100, ImgUrl="https://images-eu.ssl-images-amazon.com/images/I/41bT27cb-QL._AA200_.jpg" },
        //    new Product { Name = "Mouse", Price= 50,ImgUrl="https://images-eu.ssl-images-amazon.com/images/I/31lpICGY6xL._AA200_.jpg" },
        //    new Product { Name = "Monitör", Price= 1500,ImgUrl="https://images-eu.ssl-images-amazon.com/images/I/515dTbnFwFL._AA200_.jpg" },
        //};

        // GET api/Product
        [HttpGet]
        public ActionResult<IEnumerable<Product>> Get()
        {
            //foreach (var item in productList)
            //{
            //    _context.Products.Add(item);
            //    _context.SaveChanges();
            //}
            return _context.Products.ToList();
        }

        // GET api/Product/5
        [HttpGet("{id}")]
        public ActionResult<Product> Get(int id)
        {
            return _context.Products.FirstOrDefault(x => x.ProductId == id);
        }

        // POST api/Product
        [HttpPost]
        public void Post([FromBody] Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();
        }

        // PUT api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Product product)
        {
            var oldProduct = _context.Products.FirstOrDefault(x => x.ProductId == id);
            oldProduct.Name = product.Name;
            oldProduct.Price = product.Price;
            _context.Products.Update(oldProduct);
            _context.SaveChanges();
        }

        // DELETE api/Product/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var product = _context.Products.FirstOrDefault(x => x.ProductId == id);
            _context.Products.Remove(product);
            _context.SaveChanges();
        }
    }
}
